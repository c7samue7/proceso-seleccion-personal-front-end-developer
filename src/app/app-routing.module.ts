import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './auth/components/login/login.component';
import { RegisterComponent } from './auth/components/register/register.component';
import { ListarSubscriberComponent } from './components/subscriber/listar/listar-subscriber/listar-subscriber.component';
import { CrearSubscriberComponent } from './components/subscriber/crear/crear-subscriber/crear-subscriber.component';
import { CountrieComponent } from './components/countrie/countrie.component';

const routes: Routes = [
  {path: '',redirectTo: '/auth', pathMatch:'full'},
  {path: 'auth',component:LoginComponent},
  {path: 'auth/register',component:RegisterComponent},
  //------------------------ Subs
  {path: 'listar-subscriptor', component: ListarSubscriberComponent},
  {path: 'crear-subscriptor', component: CrearSubscriberComponent},
  {path: 'editar-subscriptor/:id',component: CrearSubscriberComponent},  
  //------------------------ countries
  {path: 'listar-countries', component:CountrieComponent},
  {path: '**',redirectTo: '/auth', pathMatch:'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
