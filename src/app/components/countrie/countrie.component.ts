import { Component, OnInit } from '@angular/core';
import { CountrieService } from 'src/app/services/countries.service'; 
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { FormGroup,FormBuilder,FormControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-countrie',
  templateUrl: './countrie.component.html',
  styleUrls: ['./countrie.component.css']
})
export class CountrieComponent implements OnInit {
  filter="";
  listcountries: any[] = [];
  public totalItems = 0;
  public page = 0;  
  public rowsOnPage = 9;
  public math = Math.ceil;
  constructor(private CountrieService: CountrieService,
              private router:Router) {
   }
  ngOnInit(): void {
    this.obtcountries();
  }
  obtcountries() {
    this.CountrieService.get().subscribe(data => {
      console.log(data)
      this.listcountries=data.Data;
      this.totalItems = this.listcountries.length;
    }, error => {
      console.log(error);
    })
  }

  pageChange(p) {
    this.page = p;
  }

  nextPage() {
    if (this.page < this.totalItems - this.rowsOnPage) {
      this.page += 9;
    }
  }

  prevPage() {
    if (this.page > 0) {
      this.page -= 9;
    }
  }

  cancelar() {
    this.router.navigate(["/listar-subscriptor"]);
  }

  
}
function Id(Id: any) {
  throw new Error('Function not implemented.');

}
