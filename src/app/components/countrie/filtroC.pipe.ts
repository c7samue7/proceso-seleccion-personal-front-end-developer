import { Pipe, PipeTransform } from '@angular/core';

/**
 * @author Christian Araque
 */
@Pipe({
  name: 'filtroC'
})
export class filterC implements PipeTransform {

  transform(value: any, args: any,sort: any): any {
    let resultadobusqueda = [];
  for(const item of value) {
    if (
      item.Code.indexOf(args) > -1 ||
      item.Name.indexOf(args) > -1
    ) {

      resultadobusqueda.push(item);
    };
  };
      return resultadobusqueda;
      }
}