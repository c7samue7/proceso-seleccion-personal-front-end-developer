import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { SubscriberService } from 'src/app/services/subscriber.service';
import { CountrieService } from 'src/app/services/countries.service';

@Component({
  selector: 'app-crear-subscriber',
  templateUrl: './crear-subscriber.component.html',
  styleUrls: ['./crear-subscriber.component.css']
})
export class CrearSubscriberComponent implements OnInit {
  SubscriberForm: FormGroup;
  titulo = 'Create Subscriber';
  id: string | null;
  listsubs: any[] = [];
  listcountries:any[] = [];

  static SubscriberForm: any;
  constructor(private fb: FormBuilder,
    private router: Router,
    private toastr: ToastrService,
    private SubscriberService: SubscriberService,
    private aRoute: ActivatedRoute,
    private CountrieService:CountrieService) {
    this.SubscriberForm = this.fb.group(
      {
        Name: ['', Validators.required],
        Email: ['', Validators.required],
        CountryCode: ['', Validators.required],
        PhoneNumber: ['', Validators.required],
      });
    this.id = this.aRoute.snapshot.paramMap.get('id')
  }

  ngOnInit(): void {
    this.esEditar();
    this.selectedCountrie();
  }
  agregar() {

    if (this.id !== null) {
      const subscriber: any =
      {
        Id: this.id,
        Name: (this.SubscriberForm.get('Name')?.value),
        Email: (this.SubscriberForm.get('Email')?.value),
        CountryCode: (this.SubscriberForm.get('CountryCode')?.value),
        PhoneNumber: (this.SubscriberForm.get('PhoneNumber')?.value),
        Topics: []
      };
      //editamos producto
      this.SubscriberService.editar(this.id, subscriber).subscribe(data => {
        this.toastr.success('Subscriber was successfully edited!', 'Subscriber Saved!');
        this.router.navigate(["/listar-subscriptor"]);
      }, error => {
        console.log(error);
      })
    }
    else {
      const subscriber: any =
      {
        Name: (this.SubscriberForm.get('Name')?.value),
        Email: (this.SubscriberForm.get('Email')?.value),
        CountryCode: (this.SubscriberForm.get('CountryCode')?.value),
        PhoneNumber: (this.SubscriberForm.get('PhoneNumber')?.value),
        Topics: []
      };
      let Subs: any = {
        Subscribers: [subscriber]
      };
      //agregamos producto
      this.SubscriberService.crear(Subs).subscribe(data => {
        this.toastr.success('Subscriber Created Successfully!', 'Subscriber Saved!');
        this.router.navigate(["/listar-subscriptor"]);
      }, error => {
        console.log(error);
      })
    }
  }

  esEditar() {
    if (this.id !== null) {
      this.titulo = "Edit subscriber";
      this.SubscriberService.Obtener(this.id).subscribe(data => {
        this.SubscriberForm.setValue({
          Name: data.Name,
          Email: data.Email,
          CountryCode: data.CountryCode,
          PhoneNumber: data.PhoneNumber
        })
      }, error => {
        console.log(error);
      })
    }
  }

  public selectedCountrie(): void {
    this.CountrieService.get().subscribe(data => {
      this.listcountries = [];
      for (let i = 0; i < data.Data.length; i++) {
        this.listcountries[i] = { value: data.Data[i].Code, label: data.Data[i].Name};
      }
    }, error => {
      console.log(error);
    })
  }
}

