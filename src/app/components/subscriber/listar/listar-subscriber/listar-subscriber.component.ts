import { Component, OnInit } from '@angular/core';
import { SubscriberService } from 'src/app/services/subscriber.service';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { FormGroup,FormBuilder,FormControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-listar-subscriber',
  templateUrl: './listar-subscriber.component.html',
  styleUrls: ['./listar-subscriber.component.css']
})
export class ListarSubscriberComponent implements OnInit {
  filter="";
  Search="";
  listsubs: any[] = [];
  SubscriberForm: FormGroup;
  formGroup="";
  public totalItems = 0;
  public page = 0;
  public rowsOnPage = 9;
  public math = Math.ceil;
  constructor(private SubscriberService: SubscriberService,
              private AuthService:AuthService,
              private router:Router,
              private fb: FormBuilder,
              private toastr: ToastrService
  ) {
    this.SubscriberForm = this.fb.group({
      Id: [''],
    });
   }



  ngOnInit(): void {
    this.obtsubs();
    this.SubscriberForm=new FormGroup({
      Id:new FormControl(''),
      })

  }
  obtsubs() {
    this.SubscriberService.get().subscribe(data => {
      this.listsubs=data.Data;
      this.totalItems = data.Count;
    }, error => {
      console.log(error);
    })
  }

  obtsub() {
    this,this.pageChange(0);
    const _Id = this.SubscriberForm.value.Id;

    this.SubscriberService.Obtener(_Id).subscribe(data => {
      console.log("totalItems--->",data)
      this.listsubs=[data];
      this.totalItems = 1;
    }, error => {
      console.log(error);
    })
    this.completeLogin();
  }

  completeLogin(){
    this.SubscriberForm.reset();
    // calling this method will reset the method

    }

    eliminarSubs(id: any){
      var r = confirm("sure you want to remove the subscriber?");
      if (r == true) {
          this.SubscriberService.eliminar(id).subscribe(data=>{
          this.toastr.error('The Subscriber was successfully removed!','Deleted subscriber!');
          this.obtsubs();
          }, error =>{
            console.log(error);
          })
           this.obtsubs();
    } else {
      this.toastr.success('Subscriber list unchanged!','without changes!');
      this.obtsubs();
    }

   }

  pageChange(p) {
    this.page = p;
  }

  nextPage() {
    if (this.page < this.totalItems - this.rowsOnPage) {
      this.page += 9;
    }
  }

  prevPage() {
    if (this.page > 0) {
      this.page -= 9;
    }
  }

  cancelar() {
    this.obtsubs();
  }


}
function Id(Id: any) {
  throw new Error('Function not implemented.');
}

