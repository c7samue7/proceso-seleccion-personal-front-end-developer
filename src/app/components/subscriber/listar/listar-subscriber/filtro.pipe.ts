import { Pipe, PipeTransform } from '@angular/core';

/**
 * @author Christian Araque
 */
@Pipe({
  name: 'filtro'
})
export class filter implements PipeTransform {

  transform(value: any, args: any,sort: any): any {
    let resultadobusqueda = [];
    var ide = null;
  for(const item of value) {
    ide = item.Id.toString();
    if (
      ide.indexOf(args) > -1 ||
      item.Name.indexOf(args) > -1
    ) {

      resultadobusqueda.push(item);
    };
  };
      return resultadobusqueda;
      }
}