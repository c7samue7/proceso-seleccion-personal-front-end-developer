import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { User } from '../models/auth/auth';
import { JwtResponse } from '../models/auth/jwt-response';
import { Observable, BehaviorSubject,tap } from 'rxjs';



@Injectable()
export class AuthService{
  url : string= 'https://lab.invertebrado.co/api/'
  authSubject=new BehaviorSubject(false);
  private token:string;

  constructor(private http:HttpClient) {

   }

  register(user: User): Observable<JwtResponse>{
    return this.http.post<JwtResponse>( `${this.url}register`,
    user).pipe(tap(
      (res:JwtResponse)=>{
        if(res){
          //guardar Token
          // this.saveToken(res.dataUser.accessToken,res.dataUser.expiresIn)
        }
      }

    ));
  }  

  login(user: any): Observable<any>{
    return this.http.post<any>(`${this.url}account/login`,
    user).pipe(tap(
      (res)=>{
        if(res){
          //guardar Token          
          this.saveToken(res.Token)
        }
      }
    ));
  }  

  logouth(){
    this.token='';
    localStorage.removeItem("Token");
  }

  public saveToken(token:any):void{
    this.token=token
    localStorage.setItem("Token",this.token); 
  }

  public getToken():string{
    if(!this.token){
      this.token=localStorage.getItem("Token");
    }
    return this.token;
  }
}

