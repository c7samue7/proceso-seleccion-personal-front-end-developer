import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core'; 
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})

export class CountrieService {

  headers = new HttpHeaders();
  
  constructor(private http: HttpClient) { 
  }
  url = 'https://lab.invertebrado.co/api/countries/';

  get():Observable<any>{
    return this.http.get(this.url,{headers:this.headers.append('Authorization', "Bearer "+ localStorage.getItem("Token"))});
  }
}