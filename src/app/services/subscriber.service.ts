import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core'; 
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})

export class SubscriberService {

  headers = new HttpHeaders();
  
  constructor(private http: HttpClient) { 
  }
  url = 'https://lab.invertebrado.co/api/subscribers/';

  get():Observable<any>{
    return this.http.get(this.url,{headers:this.headers.append('Authorization', "Bearer "+ localStorage.getItem("Token"))});
  }

  eliminar(id:string):Observable<any>{
    return this.http.delete(this.url+id,{headers:this.headers.append('Authorization', "Bearer "+ localStorage.getItem("Token"))});
  }

  crear(Subscribers: any): Observable<any>{
    return this.http.post(this.url,Subscribers,{headers:this.headers.append('Authorization', "Bearer "+ localStorage.getItem("Token"))});
  }

  editar(id:string, subscriber:any): Observable<any>{
    console.log("subscriber--->",subscriber)
    return this.http.put(this.url+id,subscriber,{headers:this.headers.append('Authorization', "Bearer "+ localStorage.getItem("Token"))});
  }

  Obtener(id:any):Observable<any>{
    return this.http.get(this.url+id,{headers:this.headers.append('Authorization', "Bearer "+ localStorage.getItem("Token"))});
  }
}