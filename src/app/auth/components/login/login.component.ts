import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { User } from 'src/app/models/auth/auth'; 
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private authService:AuthService,
              private router:Router,
              private toastr: ToastrService) { }

  ngOnInit(): void {
  }

  onLogin(form):void{
    this.authService.login(form.value).subscribe(res=>{
      this.toastr.success('A ingresado con exito!','Login Exitoso!');
      this.router.navigateByUrl('/listar-subscriptor');
    }, error =>{
      this.toastr.error('Usuario o Contraseña incorrectas!','Login Fallido!');
      console.log(error);
    })
   }
}
