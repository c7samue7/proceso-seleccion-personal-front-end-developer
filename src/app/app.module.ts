import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { ToastrModule } from 'ngx-toastr';
import { FormsModule,} from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { TooltipModule } from 'ng2-tooltip-directive';
//componentes
import { AppComponent } from './app.component';
import { FooterComponent } from './components/footer/footer/footer.component';
import { HeaderComponent } from './components/header/header/header.component';
import { LoginComponent } from './auth/components/login/login.component';
import { RegisterComponent } from './auth/components/register/register.component';
import { AuthService } from './services/auth.service';
import { ListarSubscriberComponent } from './components/subscriber/listar/listar-subscriber/listar-subscriber.component';
import { CrearSubscriberComponent } from './components/subscriber/crear/crear-subscriber/crear-subscriber.component';
import { paginacion } from './components/subscriber/listar/listar-subscriber/paginacion.pipe';
import { filter } from './components/subscriber/listar/listar-subscriber/filtro.pipe';
import { Sort } from './components/subscriber/listar/listar-subscriber/sort.pipe';
import { CountrieComponent } from './components/countrie/countrie.component';
import { filterC } from './components/countrie/filtroC.pipe';


@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    HeaderComponent,
    LoginComponent,
    RegisterComponent,
    ListarSubscriberComponent,
    CrearSubscriberComponent,
    paginacion,
    filter,
    Sort,
    CountrieComponent,
    filterC,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    HttpClientModule,
    FormsModule,
    TooltipModule
  ],
  providers: [AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
