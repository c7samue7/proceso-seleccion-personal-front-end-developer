export interface JwtResponse {
    dataUser:{
        id:number,
        UserName:string,
        Password:string,
        Token:string,
        Status: number,
        Permissions:any[],
        Locations:any[],
        LastLocationId:number,
        Preferences:any[],
        UserType:string,
        Email:string,
        FirstName:string,
        LastName:string,
        CompanyName:string,
        TimeZoneInfo:Date,
    }
}